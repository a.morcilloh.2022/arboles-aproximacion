#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.
'''

import sys

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):

    productions = []
    for num in range(int(min_trees), int(max_trees + 1)):
        anadir=(num, compute_trees(num))
        productions.append(anadir)
    return productions

def read_arguments():

    if len(sys.argv) != 6:
        print("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
        sys.exit(0)
    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])
    except ValueError:
        print("All arguments must be integers")
        sys.exit(0)
    return base_trees, fruit_per_tree, reduction, min, max


def main():

    global base_trees, fruit_per_tree, reduction

    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)

    produccion_max = 0
    arboles = 0

    for i in range(0, len(productions)):
        print(productions[i][0], productions[i][1])
        if productions[i][1] > produccion_max:
            produccion_max = productions[i][1]
            arboles = productions[i][0]

    print(f"Best production: {produccion_max}, for {arboles} trees")


if __name__ == '__main__':
    main()
